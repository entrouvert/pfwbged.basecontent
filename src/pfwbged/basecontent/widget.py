from zope.interface import implementer
import z3c.form.interfaces

from collective.z3cform.chosen.widget import AjaxChosenSelectionWidget


@implementer(z3c.form.interfaces.IFieldWidget)
def AjaxChosenFieldWidget(field, request):
    widget = z3c.form.widget.FieldWidget(field,
        AjaxChosenSelectionWidget(request))
    widget.populate_select = True
    widget.ignoreMissing = True
    return widget

@implementer(z3c.form.interfaces.IFieldWidget)
def ReallyAjaxChosenFieldWidget(field, request):
    widget = z3c.form.widget.FieldWidget(field,
        AjaxChosenSelectionWidget(request))
    widget.populate_select = False
    widget.ignore_missing = True
    return widget
