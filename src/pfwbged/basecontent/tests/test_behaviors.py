# -*- coding: utf8 -*-
import unittest2 as unittest

import datetime

from zope.component import getUtility

from plone.behavior.interfaces import IBehavior
from plone.autoform.interfaces import IFormFieldProvider
from plone.app.testing.helpers import setRoles
from plone.app.testing.interfaces import TEST_USER_NAME, TEST_USER_ID

from ecreall.helpers.testing.base import BaseTest

from ..testing import INTEGRATION
from pfwbged.basecontent.behaviors import IDeadline


class TestBehaviors(unittest.TestCase, BaseTest):
    """Tests behaviors"""

    layer = INTEGRATION

    def setUp(self):
        super(TestBehaviors, self).setUp()
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.login(TEST_USER_NAME)
        self.portal.invokeFactory('testtype', 'testitem')
        self.testitem = self.portal['testitem']

    def test_behaviors_installation(self):
        deadline_behavior = getUtility(IBehavior,
                name='pfwbged.basecontent.behaviors.IDeadline')
        self.assertEqual(deadline_behavior.interface, IDeadline)
        IFormFieldProvider.providedBy(deadline_behavior.interface)

    def test_deadline_fields(self):
        item = self.testitem
        self.assertTrue(hasattr(item, 'deadline'))
        #date = datetime.datetime.today() + datetime.timedelta(days=3)
        #hour = datetime.time(18, 0)
        #default_deadline = datetime.datetime.combine(date, hour)
        #self.assertEqual(item.deadline, default_deadline)
        item.deadline = datetime.datetime(2013, 7, 1, 15, 8)
        self.assertEqual(item.deadline, datetime.datetime(2013, 7, 1, 15, 8))
