import datetime

from zope import schema
from zope.interface import alsoProvides

from plone.autoform.interfaces import IFormFieldProvider
from plone.autoform import directives as form
from plone.directives.form import default_value
from plone.formwidget.datetime.z3cform.widget import DatetimeFieldWidget
from plone.supermodel import model

from collective.dms.basecontent.relateddocs import RelatedDocs
from collective.dms.basecontent.widget import AjaxChosenMultiFieldWidget
from collective.dms.thesaurus.keywordsfield import ThesaurusKeywords

from pfwbged.basecontent import _


class IDeadline(model.Schema):
    """Deadline behavior"""

    deadline = schema.Datetime(
        title=_(u"Deadline"),
        required=True,
        )
    form.widget(deadline=DatetimeFieldWidget)


@default_value(field=IDeadline['deadline'])
def deadlineDefaultValue(data):
    """Default value for deadline field today+3 days at 12:00"""
    date = datetime.datetime.today() + datetime.timedelta(days=3)
    hour = datetime.time(12, 0)
    return datetime.datetime.combine(date, hour)


class IRelatedTask(model.Schema):
    """Behavior to add a relation to the related task in the outgoing mail"""

    related_task = RelatedDocs(title=_("Related task"),
                               display_backrefs=False,
                               required=False,
                               )

    form.mode(related_task='hidden')


class IPfwbDocument(model.Schema):
    """Fields that are specific to Pfwb document"""

    treated_by = schema.List(title=_(u"Treated by"),
                             required=False,
                             value_type=schema.Choice(vocabulary=u'collective.dms.basecontent.treating_groups')
                             )

    keywords = ThesaurusKeywords(title=_(u'Keywords'),
                                 required=False)

    form.widget(treated_by=AjaxChosenMultiFieldWidget)
    form.order_before(treated_by='treating_groups')


class IPfwbIncomingMail(model.Schema):
    """Fields that are specific to Pfwb incoming mail"""

    in_copy = schema.List(title=_(u"In copy"),
                          required=False,
                          value_type=schema.Choice(vocabulary=u'collective.dms.basecontent.recipient_groups')
                          )

    form.widget(in_copy=AjaxChosenMultiFieldWidget)
    form.order_after(in_copy='treated_by')
    form.order_before(in_copy='treating_groups')


alsoProvides(IDeadline, IFormFieldProvider)
alsoProvides(IRelatedTask, IFormFieldProvider)
alsoProvides(IPfwbDocument, IFormFieldProvider)
alsoProvides(IPfwbIncomingMail, IFormFieldProvider)
