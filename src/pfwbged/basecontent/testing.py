# -*- coding: utf8 -*-

from plone.app.testing import PloneWithPackageLayer
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting

import pfwbged.basecontent

DMS = PloneWithPackageLayer(
    zcml_filename="testing.zcml",
    zcml_package=pfwbged.basecontent,
    gs_profile_id='pfwbged.basecontent:testing',
    name="DMS")

DMS_TESTS_PROFILE = PloneWithPackageLayer(
    bases=(DMS, ),
    zcml_filename="testing.zcml",
    zcml_package=pfwbged.basecontent,
    gs_profile_id='pfwbged.basecontent:testing',
    name="DMS_TESTS_PROFILE")

INTEGRATION = IntegrationTesting(
    bases=(DMS_TESTS_PROFILE,), name="INTEGRATION")

FUNCTIONAL = FunctionalTesting(
    bases=(DMS_TESTS_PROFILE,), name="FUNCTIONAL")
