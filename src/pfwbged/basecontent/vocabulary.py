from five import grok

from zope.schema.interfaces import IVocabularyFactory
from zope.schema.vocabulary import SimpleVocabulary

from Products.CMFCore.utils import getToolByName

from pfwbged.basecontent import _


class AdministrativeThesaurusSource(grok.GlobalUtility):
    grok.name("pfwbged.basecontent.thesaurus")
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        catalog = getToolByName(context, 'portal_catalog')
        results = catalog(portal_type='dmskeyword')
        keywords = [x.getObject() for x in results]
        def cmp_keyword(x, y):
            return cmp(x.title, y.title)
        keywords.sort(cmp_keyword)
        keyword_terms = [SimpleVocabulary.createTerm(
                                x.id, x.id, x.title) for x in keywords]
        return SimpleVocabulary(keyword_terms)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'


class InsuranceDocumentTypes(grok.GlobalUtility):
    grok.name('InsuranceDocumentTypes')
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        terms = []
        types = {'green_card': _('Green Card'),
                 'car_insurance': _('Car Insurance'),
                 'healthcare_card': _('Healthcare Card')
                }
        for (token, value) in types.iteritems():
            term = SimpleVocabulary.createTerm(token, token, value)
            terms.append(term)
        return SimpleVocabulary(terms)


class Sessions(grok.GlobalUtility):
    grok.name('Sessions')
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        terms = []
        # TODO: add a control panel to encode sessions
        return SimpleVocabulary(terms)
