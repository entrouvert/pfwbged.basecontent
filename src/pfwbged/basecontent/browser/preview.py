import json

from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName

from plone import api

from collective.documentviewer.settings import Settings
from collective.documentviewer.settings import GlobalSettings
from collective.documentviewer import storage



class PreviewView(BrowserView):
    def __call__(self):
        result = {}
        portal_catalog = getToolByName(self.context , 'portal_catalog')
        folder_path = '/'.join(self.context.getPhysicalPath())
        results = portal_catalog.searchResults({'path': {'query': folder_path},
            'portal_type': 'dmsmainfile',
            'sort_on': 'getObjPositionInParent',
            'sort_order': 'descending'})

        first_file = None
        if results:
            for i in range(len(results)):
                first_file = results[i].getObject()
                if first_file.file is not None:
                    break
            else:
                first_file = None

        if first_file:
            result = {'filename': first_file.file.filename}
            settings = Settings(first_file)
            result['num_pages'] = settings.num_pages
            if settings.num_pages > 0:
                global_settings = GlobalSettings(api.portal.get())

                resource_url = global_settings.override_base_resource_url
                rel_url = storage.getResourceRelURL(gsettings=global_settings,
                        settings=settings)
                if resource_url:
                    dvpdffiles = '%s/%s' % (resource_url.rstrip('/'), rel_url)
                else:
                    dvpdffiles = '%s/%s' % (api.portal.get().absolute_url(), rel_url)

                image_format = settings.pdf_image_format
                if not image_format:
                    image_format = global_settings.pdf_image_format

                result['thumbnail_url'] = '%s/normal/dump_1.%s' % (dvpdffiles,
                        image_format)

        self.request.response.setHeader('Content-type', 'application/json')
        return json.dumps(result)
