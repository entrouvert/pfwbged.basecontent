from collective.dms.mailcontent.dmsmail import DmsOutgoingMail
from collective.dms.mailcontent.dmsmail import IDmsOutgoingMail
from zope import schema
from zope.interface import implements

from plone.autoform import directives as form
from plone.supermodel import model

from collective.contact.core.schema import ContactList, ContactChoice
from collective.dms.basecontent.dmsdocument import IDmsDocument, DmsDocument
from collective.dms.mailcontent.dmsmail import IDmsIncomingMail, DmsIncomingMail

from pfwbged.basecontent import _

from .widget import AjaxChosenFieldWidget, ReallyAjaxChosenFieldWidget


class IPfwbBaseDocument(IDmsDocument):
    original_paper_version = schema.Choice(
            title=_(u'Original Paper Version'),
            required=False,
            vocabulary='collective.dms.basecontent.recipient_groups')
    form.widget(original_paper_version=AjaxChosenFieldWidget)


class PfwbBaseDocument(DmsDocument):
    implements(IPfwbBaseDocument)


class IOrderNumber(model.Schema):
    order_number = schema.Int(
            title=_(u'Order number'),
            required=False)


class IMedicalCertificate(IDmsIncomingMail):
    """ """
    concerned_contact = ContactChoice(
            title=_(u'Concerned Person'),
            portal_types=('person',),
            required=False)

    concerned_person = schema.Choice(
            title=_(u'Concerned Person (do not use anymore)'),
            required=False,
            vocabulary='plone.principalsource.Users')
    form.widget(concerned_person=ReallyAjaxChosenFieldWidget)

    start_date = schema.Date(
            title=_(u'Start Date'),
            required=False)
    end_date = schema.Date(
            title=_(u'End Date'),
            required=False)


class MedicalCertificate(DmsIncomingMail):
    implements(IMedicalCertificate)


class IPressRelease(IPfwbBaseDocument):
    """ """
    contacts = ContactList(
            title=_(u'Contacts'),
            required=False)

class PressRelease(PfwbBaseDocument):
    implements(IPressRelease)


class IMeetingDate(IPfwbBaseDocument):
    """ """
    meeting_date = schema.Date(
            title=_(u'Meeting Date'),
            required=False)


class IAgenda(IMeetingDate):
    """ """

class Agenda(PfwbBaseDocument):
    implements(IAgenda)


class IBoardDecision(IPfwbBaseDocument, IOrderNumber):
    """ """
    meeting_date = schema.Date(
            title=_(u'Meeting Date'),
            required=False)

    immediate = schema.Bool(
            title=_(u'Immediate Decision'),
            default=False)


class BoardDecision(PfwbBaseDocument):
    implements(IBoardDecision)


class IGreenBoardDecision(IMeetingDate):
    """ """


class GreenBoardDecision(PfwbBaseDocument):
    implements(IGreenBoardDecision)


class IInsuranceDocument(IPfwbBaseDocument):
    """ """
    person = ContactChoice(
            title=_(u'Person'),
            required=False)

    company = schema.TextLine(
            title=_(u'Insurance Company'),
            required=False)

    company_reference = schema.TextLine(
            title=_(u'Insurance Company Reference'),
            required=False)

    document_type = schema.Choice(
            title=_(u'Document Type'),
            vocabulary='InsuranceDocumentTypes')


class InsuranceDocument(PfwbBaseDocument):
    implements(IInsuranceDocument)


class IInvoice(IDmsIncomingMail):
    """ """


class Invoice(DmsIncomingMail):
    implements(IInvoice)


class INoteForBoard(IPfwbBaseDocument, IOrderNumber):
    """ """
    internal_reference_no = schema.TextLine(
            title=_(u'Internal Reference Number'),
            required=False)


class NoteForBoard(PfwbBaseDocument):
    implements(INoteForBoard)


class IInternalNote(IPfwbBaseDocument):
    internal_reference_no = schema.TextLine(
            title=_(u'Internal Reference Number'),
            required=False)

    sender = ContactChoice(
            title=_(u'Sender'),
            required=False)

    recipients = ContactList(
            title=_(u'Recipients'),
            required=False)

    session = schema.Choice(title=_(u'Session'),
            vocabulary='Sessions', required=False)


class InternalNote(PfwbBaseDocument):
    implements(IInternalNote)


class IMinutes(IDmsDocument):
    reference = schema.TextLine(
            title=_(u'Internal Reference'),
            required=False)

    meeting_date = schema.Date(
            title=_(u'Meeting Date'),
            required=False)


class Minutes(DmsDocument):
    implements(IMinutes)


class IBoardMinutes(IDmsDocument):
    reference = schema.TextLine(
            title=_(u'Internal Reference'),
            required=False)

    meeting_date = schema.Date(
            title=_(u'Meeting Date'),
            required=False)


class BoardMinutes(DmsDocument):
    implements(IBoardMinutes)


class IMemorandum(IDmsOutgoingMail):
    """
    """


class Memorandum(DmsOutgoingMail):
    implements(IMemorandum)


class INotice(IDmsOutgoingMail):
    """
    """


class Notice(DmsOutgoingMail):
    implements(INotice)


class IInformationNote(IDmsOutgoingMail):
    """
    """


class InformationNote(DmsOutgoingMail):
    implements(IInformationNote)


class ICopNote(IDmsOutgoingMail):
    """
    """

    form.omitted("recipients")


class CopNote(DmsOutgoingMail):
    implements(ICopNote)


class ICopMinutes(IDmsOutgoingMail):
    """
    """

    form.omitted("recipients")


class CopMinutes(DmsOutgoingMail):
    implements(ICopMinutes)
