#! -*- coding: utf8 -*-
from setuptools import setup, find_packages

version = '1.0'

setup(name='pfwbged.basecontent',
      version=version,
      description="Base content types for PFWB document management system",
      # Get more strings from
      # http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=["Programming Language :: Python",
                   ],
      keywords='',
      author='Frédéric Péters',
      author_email='fpeters@entrouvert.com',
      license='gpl',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      namespace_packages=['pfwbged'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
          'ecreall.helpers.upgrade',
          'five.grok',
          'collective.contact.core',
          'collective.dms.thesaurus',
          'collective.dms.basecontent',
          'collective.task',
          'collective.z3cform.chosen',
          'plone.api',
          'plone.app.dexterity',
          'plone.formwidget.datetime',
      ],
      extras_require={
          'test': ['plone.app.testing',
                   'ecreall.helpers.testing',
                   'plone.app.vocabularies'
                   ],
          },
      entry_points="""
      # -*- Entry points: -*-
      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
